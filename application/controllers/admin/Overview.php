<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Overview extends CI_Controller {
    public function __construct()
    {
		parent::__construct();
        if(!$this->session->userdata('email')) {
            redirect('auth');
        }
        $this->load->model("User_model");
        $this->load->library('form_validation');
    }
	

	public function index()
	{
	// load view admin/overview.php
	$data['admin'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
	$this->load->view("admin/overview", $data);
	
	}
}