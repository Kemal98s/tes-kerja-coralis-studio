<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

	public function index()
	{   
        $data['title'] = 'Register';
        $this->load->view('auth/templates/header_login', $data);
        $this->load->view('auth/register');
		$this->load->view('auth/templates/footer_login');
    }

    public function add()
    {
        $user = $this->User_model;
        $data['title'] = 'Register';
        $validation = $this->form_validation;
        $validation->set_rules($user->rules());
        if ($validation->run()) {
            $user->save();
            $this->session->set_flashdata('success', 'Pendaftaran berhasil! Dimohon untuk cek email untuk aktivasi akun anda.');
            redirect("auth");
        }
        $this->load->view('auth/templates/header_login', $data);
        $this->load->view('auth/register');
		$this->load->view('auth/templates/footer_login');
    }
}