<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
    }

	public function index()
	{   
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        
        if ($this->form_validation->run() == false) {
        $data['title'] = 'Login';
        $this->load->view('auth/templates/header_login', $data);
        $this->load->view('auth/login');
		$this->load->view('auth/templates/footer_login');
	} else {
        $this->_login();
        }
    }

    private function _login()
    {
        $email = htmlspecialchars($this->input->post('email', TRUE));
        $password = htmlspecialchars($this->input->post('password', TRUE));
        
        $users = $this->db->get_where('user', ['email' => $email])->row_array();
        //Pengecekan acktivasi dari user
        if ($users) {
            if ($users['is_active'] == 1) {
                if (password_verify($password, $users['password'])) {
                    $data = [
                        'email' => $users['email'],
                        'name' => $users['name'],
                        'role_id' => $users['role_id']
                     ];
                    $this->session->set_userdata($data, TRUE);
                    if($users['role_id'] == 1){
                        redirect('admin/overview');
                    } else {
                        redirect('member/overview');
                    }
                } else {
                    $this->session->set_flashdata('failed', 'Wrong password!');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('failed', 'Email has not been activated!!');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('failed', 'Email is not registered!');
            redirect('auth');
        }
    }

    public function signout()
    {
        $this->session->sess_destroy();
        redirect('auth');
    }

	public function registration()
	{   
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|matches[password2]', [
            'min_length' => 'Password too short!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'trim|required|matches[password]', 
        [   
            'matches' => 'Password dont match!'
        ]);
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[user.email]',
        [
            'is_unique' => 'This email already registered'
        ]);
       
        if ($this->form_validation->run() == false) {
        $data['title'] = 'Register';
        $this->load->view('auth/templates/header_login', $data);
        $this->load->view('auth/register');
		$this->load->view('auth/templates/footer_login');
        } else {
            $email = $this->input->post('email', TRUE);
            $data = [
                'name' => htmlspecialchars($this->input->post('name', TRUE)),
                'email' => htmlspecialchars($email),
                'password' => password_hash($this->input->post('password'),PASSWORD_DEFAULT),
                'photo' => $this->_uploadPhoto(),
                'role_id' => 2,
                'is_active' => 0,
                'date_created' => time()
            ];

            $token =  base64_encode(random_bytes(32));
            $user_token= [
                'email' => $email,
                'token' => $token,
                'date_created' => time()
            ];

            $this->db->insert('user', $data);
            $this->db->insert('user_token', $user_token);

            $this->_sendEmail($token, 'verify');

            $this->session->set_flashdata('success', 'Congratulations! your account has been created. Please check email to activate your Account');
            redirect('auth');
        }
	}

    private function _sendEmail($token, $type)
    {
        $config = [
            'protocol'=> 'smtp',
            'smtp_host'=> 'ssl://smtp.googlemail.com',
            'smtp_user'=>'tes.kerja.6@gmail.com',
            'smtp_pass'=>'PasswordTes',
            'smtp_port'=> 465,
            'mailtype'=>'html',
            'charset'=>'utf-8',
            'newline'=>"\r\n"
        ];

        $this->load->library('email', $config);
        $this->email->initialize($config); 

        $this->email->from('tes.kerja.6@gmail.com','Tes Kerja');
        $this->email->to($this->input->post('email'));

        if ($type == 'verify'){
            $this->email->subject('Account Verification');
            $this->email->message('Click this link to verify your account : <h2><a href="' . base_url() . 'auth/verify?email=' . 
            $this->input->post('email') . '&token=' . urlencode($token) . '">Activate</a></h2>');            
        } elseif ($type == 'forgot') {
            $this->email->subject('Reset Password');
            $this->email->message('Click this link to reset yout password : <h2><a href="' . base_url() . 'auth/reset_password?email=' . 
            $this->input->post('email') . '&token=' . urlencode($token) . '">Reset Password</a></h2>');            
        }

        if($this->email->send()){
            return true;
        }else{
            echo $this->email->print_debugger();
            die;
        }
    }

    public function verify()
    {
        $email = $this->input->get('email');
        $token = $this->input->get('token');
        $user = $this->db->get_where('user', ['email' => $email])->row_array();

        if($user){
            $user_token =  $this->db->get_where('user_token', ['token' => $token])->row_array();

            if($user_token){
                //Dalam waktu hitungannya detik . 60*60*24 = 1 Hari
                if(time() - $user_token['date_created'] < (60*60*24)) {
                    $this->db->set('is_active', 1);
                    $this->db->where('email', $email);
                    $this->db->update('user');
                    $this->db->delete('user_token', ['email' => $email]);
                    $this->session->set_flashdata('success', 'Congratulations! has been activated');
                    redirect('auth'); 
                }else{
                    $this->db->delete('user', ['email' => $email]);
                    $this->db->delete('user_token', ['email' => $email]);

                    $this->session->set_flashdata('failed', 'Account activation failed! Expired Token.');
                    redirect('auth'); 
                }
            } else {
                $this->session->set_flashdata('failed', 'Account activation failed! Wrong token.');
                redirect('auth'); 
            }
        } else {
            $this->session->set_flashdata('failed', 'Account activation failed! Wrong email.');
            redirect('auth');
        }
    }

    public function forgot_password()
	{   
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        
        if ($this->form_validation->run() == false) {
        $data['title'] = 'Forgot Password';
        $this->load->view('auth/templates/header_login', $data);
        $this->load->view('auth/forgot_password');
		$this->load->view('auth/templates/footer_login');
    } else {

        $email = $this->input->post('email');
        $user = $this->db->get_where('user',['email' => $email, 'is_active' => 1])->row_array();
            
            if ($user) {
                $token =  base64_encode(random_bytes(32));
                $user_token= [
                'email' => $email,
                'token' => $token,
                'date_created' => time()
            ];

            $this->db->insert('user_token', $user_token);

            $this->_sendEmail($token, 'forgot');

            $this->session->set_flashdata('success', 'Please check email to reset your password!');
            redirect('auth/forgot_password');

            } else {
                $this->session->set_flashdata('failed', 'Email is not registered or activated!');
                redirect('auth/forgot_password');
            }
        }   
    }

    public function reset_password(){
        $email = $this->input->get('email');
        $token = $this->input->get('token');

        $user = $this->db->get_where('user', ['email' => $email])->row_array();

        if ($user) {
            $user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();
            if ($user_token) {
                $this->session->set_userdata('reset_email', $email);
                $this->change_password();
            } else {
                $this->session->set_flashdata('failed', 'Reset password failed! Wrong token.');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('failed', 'Reset password failed! Wrong email.');
            redirect('auth');
        }
    }

    public function change_password()
    {
        if (!$this->session->userdata('reset_email')){
            redirect('auth');
        }
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|matches[password2]', [
            'min_length' => 'Password too short!'
        ]);
        $this->form_validation->set_rules('password2', 'Password', 'trim|required|matches[password]', [
            'matches' => 'Password dont match!'
        ]);
        if ($this->form_validation->run() == false) {
            $data['title']='Change Password';
            $this->load->view('auth/templates/header_login', $data);
            $this->load->view('auth/change_password');
            $this->load->view('auth/templates/footer_login');
        } else {
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $email = $this->session->userdata('reset_email');

            $this->db->set('password' , $password);
            $this->db->where('email', $email);
            $this->db->update('user');

            $this->session->unset_userdata('reset_email');
            $this->session->set_flashdata('success', 'Congratulations! update your password.');
            redirect('auth');
        }
    }

    

    private function _uploadPhoto()
    {
    $config['upload_path']          = './upload/user/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['file_name']            = time();
    $config['overwrite']			= true;
    $config['max_size']             = 3024; // 1MB
    // $config['max_width']            = 1024;
    // $config['max_height']           = 768;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('photo')) {
        return $this->upload->data("file_name");
    }
    return "default.jpg";
    }

}

?> 