<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    private $_db = "user";
    public $email;
    public $name;
    public $password;
    public $photo = "default.jpg";

    public function rules(){
        return [
            ['field' => 'name',
            'label' => 'Name',
            'rules' => 'required|trim'],

            ['field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required|min_length[6]|matches[password2]',
            'min_length' => 'Password too short!'],

            ['field' => 'password2',
            'label' => 'Password',
            'rules' => 'trim|required|min_length[6]|matches[password]',
            'matches' => 'Password dont match!'],
            
            ['field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|is_unique[user.email]']
        ];
    }

    public function save()
    {
        $post = $this->input->post();
        $this->name = $post["name"];
        $this->password = $post["password"];
        $this->email = $post["email"];
        $this->photo = $this->_uploadPhoto();
        $this->db->insert($this->_db, $this);
    }

    private function _uploadPhoto()
    {
    $config['upload_path']          = './upload/user/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['file_name']            = $this->name;
    $config['overwrite']			= true;
    $config['max_size']             = 3024; // 1MB
    // $config['max_width']            = 1024;
    // $config['max_height']           = 768;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('photo')) {
        return $this->upload->data("file_name");
    }
    return "default.jpg";
    }

}