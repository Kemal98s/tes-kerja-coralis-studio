<div class="utama">
	<div class="kartu justify-content-center">
        <!-- flashdata sweetalert2 -->
        <div class="flash-success" data-flashdata="<?= $this->session->flashdata('success'); ?>"></div>
        <div class="flash-failed" data-flashdata="<?= $this->session->flashdata('failed'); ?>"></div>
        <!---->
		<div class="col-md-12">
			<div class="py-5">
		        <h3 class="text-center mb-0">Forgot your password ?</h3>
                <hr >
				<form action="<?= base_url('auth/forgot_password') ?>" class="login-form" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label> Email </label>
                        <input  class="form-control <?php echo form_error('email') ? 'is-invalid' : '' ?>"  name="email" type="text" placeholder="Email Address" value="<?= set_value('email');?>">
                        <div class="invalid-feedback">
                            <?php echo form_error('email') ?>
                        </div>
                </div>
                <br >
                <div class="form-group">
                    <button type="submit" class="btn form-control btn-primary px-3">Reset Password</button>
                </div>
                </form>
                <hr >
                <div class="w-100 text-center mt-4 text">
                    <p class="mb-0">Already have an account?</p>
                    <a href="<?= base_url('auth') ?>">Back to login</a>
                </div>
	        </div>
		</div>
	</div>
</div>