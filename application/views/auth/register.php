<div class="utama">
	<div class="kartu justify-content-center">
        <!-- flashdata sweetalert2 -->
        <div class="flash-data" data-flashdata="<?= $this->session->flashdata('success'); ?>"></div>
        <!---->
		<div class="col-md-12">
			<div class="py-5">
		        <h3 class="text-center mb-0">Crate an Account</h3>
                <hr >
				<form action="<?= base_url('auth/registration') ?>" class="login-form" method="post" enctype="multipart/form-data"> 
                <div class="form-group">
                     <label>Name </label>
                    <input class="form-control <?php echo form_error('name') ? 'is-invalid' : '' ?>" name="name" type="text" placeholder="Full Name" value="<?= set_value('name');?>">
                    <div class="invalid-feedback">
                        <?php echo form_error('name') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label> Email </label>
                        <input  class="form-control <?php echo form_error('email') ? 'is-invalid' : '' ?>"  name="email" type="text" placeholder="Email Address" value="<?= set_value('email');?>">
                        <div class="invalid-feedback">
                            <?php echo form_error('email') ?>
                        </div>
                </div>
                <div class="form-group">
                    <label>Password </label>
                    <input  class="form-control <?php echo form_error('password') ? 'is-invalid' : '' ?>"  name="password" type="password" placeholder="Password" value="<?= set_value('password');?>">
                    <div class="invalid-feedback">
                        <?php echo form_error('password') ?>
                    </div>
                </div>
                <div class="form-group">
                     <label>Repeat Password </label>
                    <input  class="form-control <?php echo form_error('password2') ? 'is-invalid' : '' ?>"  name="password2" type="password" placeholder="Repeat Password" value="<?= set_value('password2');?>">
                    <div class="invalid-feedback">
                        <?php echo form_error('password2') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label>Photo</label>
                    <input  class="form-control-file <?php echo form_error('photo') ? 'is-invalid' : '' ?>"  name="photo" type="file" accept="image/x-png,image/gif,image/jpeg"  value="<?= set_value('photo');?>" required>
                    <div class="invalid-feedback">
                        <?php echo form_error('photo') ?>
                    </div>    
                </div>
                <br >
                <div class="form-group">
                    <button type="submit" class="btn form-control btn-primary px-3">Register Account</button>
                </div>
                </form>
                <hr >
                <div class="w-100 text-center mt-4 text">
                    <p class="mb-0">Already have an account?</p>
                    <a href="<?= base_url('auth') ?>">Back to login</a>
                </div>
	        </div>
		</div>
	</div>
</div>