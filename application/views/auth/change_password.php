<div class="utama">
	<div class="kartu justify-content-center">
        <!-- flashdata sweetalert2 -->
        <div class="flash-success" data-flashdata="<?= $this->session->flashdata('success'); ?>"></div>
        <div class="flash-failed" data-flashdata="<?= $this->session->flashdata('failed'); ?>"></div>
        <!---->
		<div class="col-md-12">
			<div class="py-5">
		        <h4 class="text-center mb-0">Change your password for  ?</h4>
                <p class="text-center mb-0"> <?= $this->session->userdata('reset_email'); ?></p>
                <hr >
				<form action="<?= base_url('auth/change_password') ?>" class="login-form" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Password </label>
                    <input  class="form-control <?php echo form_error('password') ? 'is-invalid' : '' ?>"  name="password" id="password" type="password" placeholder="New Password">
                    <div class="invalid-feedback">
                        <?php echo form_error('password') ?>
                    </div>
                </div>
                <div class="form-group">
                     <label>Repeat Password </label>
                    <input  class="form-control <?php echo form_error('password2') ? 'is-invalid' : '' ?>"  name="password2" type="password" placeholder="Repeat Password">
                    <div class="invalid-feedback">
                        <?php echo form_error('password2') ?>
                    </div>
                </div>
                <br >
                <div class="form-group">
                    <button type="submit" class="btn form-control btn-primary px-3">Reset Password</button>
                </div>
                </form>
                <hr >
                <div class="w-100 text-center mt-4 text">
                    <p class="mb-0">Already have an account?</p>
                    <a href="<?= base_url('auth') ?>">Back to login</a>
                </div>
	        </div>
		</div>
	</div>
</div>