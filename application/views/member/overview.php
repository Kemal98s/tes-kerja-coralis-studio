<!DOCTYPE html>
<html lang="en">

<head>

	<title>Tes Kerja - Dashboard Member</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="author" content="Codedthemes" />

	<!-- Favicon icon -->
	<link rel="icon" href="<?= base_url()?>assets/flashable/assets/images/favicon.ico" type="image/x-icon">
	<!-- fontawesome icon -->
	<link rel="stylesheet" href="<?= base_url()?>assets/flashable/assets/fonts/fontawesome/css/fontawesome-all.min.css">
	<!-- animation css -->
	<link rel="stylesheet" href="<?= base_url()?>assets/flashable/assets/plugins/animation/css/animate.min.css">

	<!-- vendor css -->
	<link rel="stylesheet" href="<?= base_url()?>assets/flashable/assets/css/style.css">
</head>

<body class="">
	<!-- [ Pre-loader ] start -->
	<div class="loader-bg">
		<div class="loader-track">
			<div class="loader-fill"></div>
		</div>
	</div>
	<!-- [ Pre-loader ] End -->

	<!-- [ navigation menu ] start -->
	<nav class="pcoded-navbar menupos-fixed menu-light brand-blue ">
		<div class="navbar-wrapper ">
			<div class="navbar-brand header-logo">
				<a href="<?= base_url('member/overview') ?>" class="b-brand">
					<img src="<?= base_url()?>assets/flashable/assets/images/logo.png" alt="" class="logo images">
					<img src="<?= base_url()?>assets/flashable/assets/images/logo-icon.png" alt="" class="logo-thumb images">
				</a>
				<a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
			</div>
			<div class="navbar-content scroll-div">
				<ul class="nav pcoded-inner-navbar">
					<li class="nav-item">
						<a href="<?= base_url('member/overview') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- [ navigation menu ] end -->

	<!-- [ Header ] start -->
	<header class="navbar pcoded-header navbar-expand-lg navbar-light headerpos-fixed">
		<div class="m-header">
			<a class="mobile-menu" id="mobile-collapse1" href="#!"><span></span></a>
			<a href="<?= base_url('member/overview') ?>" class="b-brand">
				<img src="<?= base_url()?>assets/flashable/assets/images/logo.png" alt="" class="logo images">
				<img src="<?= base_url()?>assets/flashable/assets/images/logo-icon.png" alt="" class="logo-thumb images">
			</a>
		</div>
		<a class="mobile-menu" id="mobile-header" href="#!">
			<i class="feather icon-more-horizontal"></i>
		</a>
		<div class="collapse navbar-collapse">
			<a href="#!" class="mob-toggler"></a>
			
			<ul class="navbar-nav ml-auto">
				<li>
					<div class="dropdown drp-user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon feather icon-settings"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right profile-notification">
							<div class="pro-head">
								<img src="<?= base_url()?>upload/user/<?= $member['photo']; ?>" style="width: 60px; height: 50px; border-radius: 100px;"
								 alt="User-Profile-Image">
								<span><?= $member['name']; ?></span>
							</div>
							<ul class="pro-body">
								<li><a href="<?= base_url('auth/signout') ?>" class="dropdown-item button-logout"><i class="feather icon-log-out"></i> Sign Out</a></li>
							</ul>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</header>
	<!-- [ Header ] end -->

	<!-- [ Main Content ] start -->
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<!-- [ breadcrumb ] start -->
							<div class="page-header">
								<div class="page-block">
									<div class="row align-items-center">
										<div class="col-md-12">
											<div class="page-header-title">
												<h5>Home</h5>
											</div>
											<ul class="breadcrumb">
												<li class="breadcrumb-item"><a href="<?= base_url('member/overview') ?>"><i class="feather icon-home"></i></a></li>
												<li class="breadcrumb-item"><a href="#!">Dashboard</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<!-- [ breadcrumb ] end -->
							<!-- [ Main Content ] start -->
							<div class="row">
								<!-- sessions-section end -->
								<div class="col-md-12 col-xl-7">
									<div class="card user-card">
										<div class="card-header">
											<h3>Profile</h3>
										</div>
										<div class="card-body  text-center">
											<div class="user-image">
												<img src="<?= base_url()?>upload/user/<?= $member['photo']; ?>" class="m-auto" 
												style=" width: 200px; height: 190px; box-shadow: 2px 2px 2px rgba(0,0,0,0.8);
												border-radius: 50px;" alt="User-Profile-Image">
											</div>
											<h4 class="f-w-600 m-t-25 m-b-10"><?= $member['name']; ?></h4>
											<h5><?= $member['email']; ?></h5>
											<hr>
											<h5 class="m-t-15">Level akses : <?php if($member['role_id'] == 1){
												echo 'Admin';
											} else {
												echo 'Member';
											}?></>
											<hr>
										</div>
									</div>
								</div>
							</div>

							<!-- [ Main Content ] end -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Required Js -->
	<script src="<?= base_url()?>assets/flashable/assets/js/vendor-all.min.js"></script>
	<script src="<?= base_url()?>assets/flashable/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?= base_url()?>assets/flashable/assets/js/pcoded.min.js"></script>
	<script src="<?php echo base_url('assets/sweetalert2/sweetalert2.all.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/sweetalert2/myscript.js') ?>"></script>

</body>

</html>