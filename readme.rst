*******************
Demo Aplikasi
*******************

Untuk melihat demo aplikasi bisa langsung kunjugi website ini :
https://tes-kerja.000webhostapp.com/


- Login sebagai admin :
- Email : tes.kerja.6@gmail.com
- Password : 123456

*catatan : Jika terjadi kendala atau website tidak bisa di akses tolong 
           beri tau saya via email ini (Kemalsholehuddin6@gmail.com) dan subject emailnya (Problem Website_Tes_Kerja)

###################
Catatan penting untuk Install website Tes Kerja
###################

- Default Projek nama file : tes_kerja
- Default Config base url : http://localhost/tes_kerja
- Default Nama database : db_tes.sql
- Default Username database : "root"
- Default password : " "  //tidak ada password

- untuk tampilan akan terlihat sama saja dengan member. hanya berbeda di level akses nya jika admin akan bertuliskan (Level  akses : Admin).
- Jika aplikasi memberikan validasi informasi ke email anda bilamana tidak ada di menu utama email, maka cek juga di email halaman spam

**************************
Fitur yang terdapat pada website
**************************

1. Login
2. Forgot Password
3. Aktivasi Akun
4. hak akses
5. Validasi

**************************
Projek Update Selanjutnya
**************************
Dikarenakan ini hanya website untuk tes kerja pada Colaris Studio, 
maka dari itu update tidak akan di lakukan.

***************
Terimakasih 
***************
- Kepada Allah S.W.T karena saya masih diberikan kesehatan untuk mengerjakan 
  tes kerja dari Colaris Studio sampai beres.
- Kepada Colaris Studio dikarenakan sudah memberikan saya peluang untuk kerja
- Kepada kedua orang tua saya yang selalu mendukung saya
