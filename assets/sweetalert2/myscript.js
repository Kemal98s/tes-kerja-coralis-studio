const success = $('.flash-success').data('flashdata');
if (success) {
    Swal.fire({
        title: '' + success,
        text: '',
        type: 'success'
    });
}

const failed = $('.flash-failed').data('flashdata');
if (failed) {
    Swal.fire({
        title: '' + failed,
        text: '',
        type: 'warning'
    });
}

$('.button-logout').on('click', function (e) {
    e.preventDefault();
    const href = $(this).attr('href');

    Swal.fire({
        title: 'Do you want to sign out?',
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    });
});